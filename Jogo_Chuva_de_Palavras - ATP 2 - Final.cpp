////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/*
	Nome: Andr� Hallwas Ribeiro Alves
	Nome: Eduardo Rodrigues Santana		 		
	Date: 22/05/16 22:02
	Descri��o: Trabalho de ATP2 Referente ao jogo, Com a implementa��o de arquivo,Buscas,E ordena��es orientadas;
*/


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#include<stdio.h>
#include<conio2.h>
#include<stdlib.h>
#include<time.h>
#include<string.h>

#define TAM_DESC 100
#define TAM_COD 10
#define TAM_PAL 30
#define TAM_NOME 30

struct ST_Palavras{
	char codpalavra[TAM_COD];
	char palavra[TAM_PAL];
	char descricao[TAM_DESC];
	int pontuacao;
	int nivel;
	char codassunto[TAM_COD];
	char status;
	int letrasativas[TAM_PAL];
};


struct ST_Assuntos{
	char codassunto[TAM_COD];
	char descricao[TAM_DESC];	
	char status;	
};

struct ST_usuarios{
	char login[TAM_COD];
	char nome[TAM_NOME];
	char tipo;
	int ponttotal;
	int level;
	///Ultimo Acesso;
	char status;
};


struct ST_Ranking{
	char login[TAM_COD];
	int ponttotal;
	char status;
};

////////////////////////////////////////////////////////////////////////////////////
///////////////////Cadastros Modifica�oes e Exclus�es///////////////////////////////
/////////////////////////|||||||||||||||||||////////////////////////////////////////

///////////////////////Estrutura de Inser��o no arquivo//////////////////
void Mod_Inserir(FILE*arq,int param);///M�dulo Geral de inser��o no arquivo, Prim�rio

///M�dulos de inser��o no arquivo, Secund�rios
void Func_Inserir_Assunto(FILE*arq,int param);
void Func_Inserir_Palavra(FILE*arq,int param);
void Func_Inserir_Usuario(FILE*arq,int param);

///M�dulos de inser��o no arquivo, Auxiliares
void Func_Digita_Assunto(FILE*arqDIG,char chave[]);
void Func_Digita_Palavra(FILE*arqDIG,char chave[]);
void Func_Digita_usuario(FILE*arqDIG,char chave[]);

///M�dulos de inser��o no arquivo, Dependentes
void Func_Escreve_Assunto(ST_Assuntos &st,char chave[]);
void Func_Escreve_Palavra(ST_Palavras &st,char chave[],char assunto[]);
void Func_Escreve_usuario(ST_usuarios &st,char chave[]);
//////////////////////////////////////////////////////////////////////////


////////////////////////Estrutura de Altera��o no arquivo/////////////////
void Mod_Altera(FILE*arq,int param);///M�dulo Geral de Altera��o no arquivo, Prim�rio

///M�dulos de Altera��o no arquivo, Secund�rios
void Func_Altera_Assunto(FILE*arq,int param);
void Func_Altera_usuario(FILE*arq,int param);
void Func_Altera_Palavra(FILE*arq,int param);

///M�dulos de Altera��o no arquivo, Auxiliares
void Func_Modifica_usuario(FILE*arqALT,int pos);
void Func_Modifica_Assunto(FILE*arqALT,int pos);
void Func_Modifica_Palavra(FILE*arqALT,int pos);


//////////////////////////////////////////////////////////////////////////

////////////////////////Estruturas de Exclus�o no arquivo/////////////////
void Mod_Deleta(FILE*arq,int param);/////Modulo geral de exclus�o, Prim�rio;

void Mod_Deleta_Fisica(FILE*arq,char aux[],int param);//////Modulo de Exclus�o Fisica de um Elemento, Secund�rio ;
void Func_Retira_Assunto_Fisica(FILE*arqEXC,char chave[]);
void Func_Retira_usuario_Fisica(FILE*arqEXC,char chave[]);
void Func_Retira_Palavra_Fisica(FILE*arqEXC,char chave[]);

void Mod_Deleta_Logica(FILE*arq,char aux[],int param);//////Modulo de Exclus�o L�gica de um Elemento, Secund�rio ;
void Func_Retira_Assunto_Logica(FILE*arqEXC,char chave[],int param);
void Func_Retira_usuario_Logica(FILE*arqEXC,char chave[],int param);
void Func_Retira_Palavra_Logica(FILE*arqEXC,char chave[],int param);

void Mod_Deleta_Fisica_Geral(FILE*arq,int param);///////////Modulo de Exclus�o Fisica de Todos os elementos excluidos l�gicamente, Secund�rio ;
void Func_Retira_Assunto_Fisica_Geral(FILE*arqEXC);
void Func_Retira_usuario_Fisica_Geral(FILE*arqEXC);
void Func_Retira_Palavra_Fisica_Geral(FILE*arqEXC);
//////////////////////////////////////////////////////////////////////////

/////////////////////////|||||||||||||||||||////////////////////////////////////////
///////////////////Cadastros Modifica�oes e Exclus�es  Fim /////////////////////////
////////////////////////////////////////////////////////////////////////////////////

////////////////////Estruturas de Relat�rios referentes aos arquivos//////
void Mod_Exibe(FILE*arq,int param);////Modulo de Exibi��o, Prim�rio;
void Alt_Exibe_Assunto(FILE*arqLEI,int tl);
void Alt_Exibe_Palavras(FILE*arqLEI,int tl);
void Alt_Exibe_usuario(FILE*arqLEI,int tl);
void Alt_Exibe_Ranking(FILE*arqLEI,int tl);
//////////////////////////////////////////////////////////////////////////

///////////////////////////////Fun��o respons�vel pelo controle Gr�fico geral dos Menus//////////////////////
void Func_Controla_Menu(char i,unsigned char Quadroci,unsigned char Quadroli);
/////////////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////Fu��es responsaveis pela exibi��o de tela///////////////////////////////////////////////////////
void Func_Muda_Tela(void);
void Gera_Tela_Intermediaria_Menu_Alt(FILE*arq,int opc);
void Gera_Tela_Menu_Alt(unsigned char Quadroci,unsigned char Quadroli,unsigned char Quadrocf,unsigned char Quadrolf,unsigned char cort,unsigned char corf);
void Gera_Tela_Cadastro(unsigned char Quadroci,unsigned char Quadroli,unsigned char Quadrocf,unsigned char Quadrolf,unsigned char cort,unsigned char corf);
void Gera_Tela_Quadro(int coli,int lini,int colf,int linf,int cort,int corf);
void Gera_Tela_Menu(unsigned char Quadroci,unsigned char Quadroli,unsigned char Quadrocf,unsigned char Quadrolf,unsigned char cort,unsigned char corf);
void Gera_Tela_Menu_ADM(unsigned char Quadroci,unsigned char Quadroli,unsigned char Quadrocf,unsigned char Quadrolf,unsigned char cort,unsigned char corf);
void Gera_Mod_Deleta_Fisica_Geral(FILE*arq);
void Gera_Tela_Menu_Login(unsigned char Quadroci,unsigned char Quadroli,unsigned char Quadrocf,unsigned char Quadrolf,unsigned char cort,unsigned char corf);
void Gera_Tela_Palavra(unsigned char Quadroci,unsigned char Quadroli,char nome[],int pos);
void Gera_Tela_Jogo(unsigned char Quadroci,unsigned char Quadroli,unsigned char Quadrocf,unsigned char Quadrolf,unsigned char cort,unsigned char corf,unsigned char level,int pontuacao,char nome[]);
void Gera_Tela_Pontuacao(unsigned char Quadroci,unsigned char Quadroli,int pontuacao);
void Gera_Tela_Level(unsigned char Quadroci,unsigned char Quadroli,unsigned char level);
void Gera_Tela_Menu_Intermediario(unsigned char Quadroci,unsigned char Quadroli,unsigned char Quadrocf,unsigned char Quadrolf,unsigned char cort,unsigned char corf);
void Gera_Tela_Intermediaria_Jogo(FILE*arq,ST_usuarios &ex,int pos);
void Gera_Tela_Menu_Exclusao(unsigned char Quadroci,unsigned char Quadroli,unsigned char Quadrocf,unsigned char Quadrolf,unsigned char cort,unsigned char corf);
void Gera_Tela_Rank(unsigned char Quadroci,unsigned char Quadroli,ST_Ranking ex);
void Gera_Tela_Leitura_Ranking(unsigned char Quadroci,unsigned char Quadroli,unsigned char Quadrocf,unsigned char Quadrolf,unsigned char cort,unsigned char corf,ST_Ranking st,int pos);
void Exibe_Top_Ranking(FILE*arqRank,ST_Ranking st);
void Func_Muda_Tela(void);
void Gera_Tela_Piscante(unsigned char Quadroci,unsigned char Quadroli,unsigned char Quadrocf,unsigned char Quadrolf,unsigned char cort,unsigned char corf);
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void Main_Menu(void);
void Alt_Menu(int i,char &tecla,FILE*arq);
void Menu_Adm(FILE*arq);
void Login(FILE*arq);
void Alt_Cadastra(FILE*arq,int param);

/////////////////////Modulos Respons�veis pela atribui��o de pontua��o e controle de level////////////////////////
unsigned char Func_Level(unsigned char lev);
unsigned char Tabela_Pontuacao(int pontuacao);
void Controle_Pontuacao(ST_usuarios &ex,int pos,int &i,int posl,int posp,char controle,char palavra[],FILE*arq);
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//////////////////////Fun��es Primarias de Controle do Jogo//////////////////////////////////////////
void Game_Engine(ST_usuarios &ex,int pos,FILE*arq);
void Main_Controle_Jogo(ST_usuarios &ex,int pos,int posp,int i,char palavra[],char &o,FILE*arq);
void Alt_Controle_Jogo(ST_usuarios &ex,char x,char &y,char Letra,int pos,char &o,FILE*arq);
/////////////////////////////////////////////////////
////////////////////////Fun��es Secund�rias//////////
void Func_Limpa_Rastro_Letra(char x,char y,int z);///Apaga o rastro de uma letra exibida na tela,para evitar o uso continuo do cls,assim aumentando o desempenho;
void Func_Retira_Letra(int posl,int &i,char palavra[]);
void Func_Conta_Letras(char palavra[],int posp,int &i,FILE*arq);
void Func_Salvar(FILE*arq,ST_usuarios ex,int pos);///Respons�vel por salvar o Jogo,salvando o registro do usuario logado;
/////////////////////////////////////////////////////////////////////////////////////////////////////

//////////////////////////Modulos de controle do Ranking////////////////////////////
void Mod_Ranking(char opc,ST_usuarios ex,FILE*arq);/////Modulo de controle geral do Ranking, Primario;
void Controle_Maior_Rank(ST_usuarios &ex,int pos,FILE*arq);
void Mod_Ranking_Alteracao(FILE*arqRANK,ST_Ranking st,char login[],ST_usuarios ex);
void Mod_Ranking_Criacao(FILE*arqRANK,FILE*arq,ST_Ranking st,ST_usuarios ex);
////////////////////////////////////////////////////////////////////////////////////

///////////////////////////Buscas//////////////////////////////////////////////
char Busca_Sequencial_Indexada_Ranking(int Tam,char chave[]);
int Busca_Letra(char nome[],char letra,int letrasativas[]);
char Busca_Sentinela_Palavra_Assunto(int Tam,char chave[]);
char Busca_Sentinela_Palavra(int Tam,char chave[]);
char Busca_Exaustiva_Assunto(int Tam,char chave[]);
char Busca_Sequencial_Indexada_Ranking(int Tam,char chave[]);
char Busca_Binario_usuario(int Tam,char chave[]);
///////////////////////////////////////////////////////////////////////////////

//////////////////////////Ordena��es///////////////////////////////////////
void OrdenaBolha_Palavra(int Tam);
void Ordenacao_Direta_Ranking(int Tam);
void OrdenaBolha_Assunto(int Tam);
void Ordenacao_Direta_usuario(int Tam);
///////////////////////////////////////////////////////////////////////////

char Busca_Sentinela(int Tam,char chave[]);///A ser Removida

//////////////Respons�vel pelo retorno do tamanho de um Arquivo////////////
int Func_Tam(int a);
///////////////////////////////////////////////////////////////////////////



////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////




void Func_Muda_Tela(void){
	
	textbackground(0);
	clrscr();
	
}



void Gera_Tela_Leitura_Ranking(unsigned char Quadroci,unsigned char Quadroli,unsigned char Quadrocf,unsigned char Quadrolf,unsigned char cort,unsigned char corf,ST_Ranking st,int pos){
	
	///Inicio tela de Leitura
	Gera_Tela_Quadro(Quadroci,Quadroli,Quadrocf,Quadrolf-21,cort,corf); ///barra de titulo "Leitura"
	
	gotoxy(Quadroci+17,Quadroli+1); //posicionamento do texto
	
	textcolor(15);
	printf("Leitura");	
	Gera_Tela_Quadro(Quadroci,Quadroli+3,Quadrocf,Quadrolf,cort,corf); //Quadro central
	Gera_Tela_Quadro(Quadroci+3,Quadroli+5,Quadrocf-3,Quadrolf-1,cort,corf); //Retangulo interior em rela��o ao quadro central
	
	gotoxy(Quadroci+5,Quadroli+5); //posicionamento do texto
	textcolor(10);
	printf("Ranking");
	
	textcolor(11);
	gotoxy(25,9);printf("Login");
	Gera_Tela_Quadro(Quadroci+5,Quadroli+8,Quadrocf-5,Quadrolf-13,cort,corf); //Campo de digita��o do texto
	gotoxy(26,11);puts(st.login);
	
	textcolor(13);
	gotoxy(33,13);printf("Posicao");
	Gera_Tela_Quadro(Quadroci+12,Quadroli+12,Quadrocf-20,Quadrolf-9,cort,corf); //Campo de digita��o do texto
	gotoxy(37,15);printf("%d",pos+1);
	
	textcolor(10);
	gotoxy(45,13);printf("Pontuacao");
	Gera_Tela_Quadro(Quadroci+22,Quadroli+12,Quadrocf-4,Quadrolf-9,cort,corf); //Campo de digita��o do texto
	gotoxy(43,15);printf("%12d",st.ponttotal);

	
	gotoxy(Quadroci+5,Quadroli+21); // posicionamento das opcoes abaixo
	
	textcolor(15);
	printf("(<-)Anterior||    ||(->)Proximo");
	
	textcolor(12);
	gotoxy(Quadroci+13,Quadroli+20);printf("(ESC)-Para Sair");
	
	textcolor(14);
	//Fim tela de Leitura
	///	gotoxy(26, 11); ///////////escrita;
	
}

void Gera_Tela_Leitura_Usuario(unsigned char Quadroci,unsigned char Quadroli,unsigned char Quadrocf,unsigned char Quadrolf,unsigned char cort,unsigned char corf,ST_usuarios st){
	
	///Inicio tela de Leitura
	Gera_Tela_Quadro(Quadroci,Quadroli,Quadrocf,Quadrolf-21,cort,corf); ///barra de titulo "Leitura"
	
	gotoxy(Quadroci+17,Quadroli+1); //posicionamento do texto
	
	textcolor(15);
	printf("Leitura");	
	Gera_Tela_Quadro(Quadroci,Quadroli+3,Quadrocf,Quadrolf,cort,corf); //Quadro central
	Gera_Tela_Quadro(Quadroci+3,Quadroli+5,Quadrocf-3,Quadrolf-1,cort,corf); //Retangulo interior em rela��o ao quadro central
	
	gotoxy(Quadroci+5,Quadroli+5); //posicionamento do texto
	textcolor(10);
	printf("Leitura");
	
	textcolor(11);
	gotoxy(25,9);printf("Nome");
	Gera_Tela_Quadro(Quadroci+5,Quadroli+8,Quadrocf-5,Quadrolf-13,cort,corf); //Campo de digita��o do texto
	gotoxy(26,11);puts(st.nome);
	
	textcolor(10);
	gotoxy(42,13);printf("Pontuacao");
	Gera_Tela_Quadro(Quadroci+22,Quadroli+12,Quadrocf-4,Quadrolf-9,cort,corf); //Campo de digita��o do texto
	gotoxy(43,15);printf("%12d",st.ponttotal);
	
	textcolor(10);
	gotoxy(26,13);printf("Login");
	Gera_Tela_Quadro(Quadroci+4,Quadroli+12,Quadrocf-20,Quadrolf-9,cort,corf); //Campo de digita��o do texto
	gotoxy(27,15);printf("%13s",st.login);
	
	textcolor(13);
	gotoxy(46,17);printf("Lv");
	Gera_Tela_Quadro(Quadroci+26,Quadroli+16,Quadrocf-4,Quadrolf-5,cort,corf); //Campo de digita��o do texto
	gotoxy(47,19);printf("%8d",st.level);
	
	textcolor(13);
	gotoxy(32,17);printf("Tipo");
	Gera_Tela_Quadro(Quadroci+8,Quadroli+16,Quadrocf-20,Quadrolf-5,cort,corf); //Campo de digita��o do texto
	gotoxy(33,19);printf("%6d",st.tipo);
	
	gotoxy(Quadroci+5,Quadroli+21); // posicionamento das opcoes abaixo
	
	textcolor(15);
	printf("(<-)Anterior||    ||(->)Proximo");
	
	textcolor(12);
	gotoxy(Quadroci+13,Quadroli+20);printf("(ESC)-Para Sair");
	
	textcolor(14);
	//Fim tela de Leitura
	///	gotoxy(26, 11); ///////////escrita;
	
}


void Gera_Tela_Assunto(unsigned char Quadroci,unsigned char Quadroli,unsigned char Quadrocf,unsigned char Quadrolf,unsigned char cort,unsigned char corf,ST_Assuntos st){
	
	///Inicio tela de Leitura
	Gera_Tela_Quadro(Quadroci,Quadroli,Quadrocf,Quadrolf-21,cort,corf); ///barra de titulo "Leitura"
	
	gotoxy(Quadroci+17,Quadroli+1); //posicionamento do texto
	
	textcolor(15);
	printf("Leitura");	
	Gera_Tela_Quadro(Quadroci,Quadroli+3,Quadrocf,Quadrolf,cort,corf); //Quadro central
	Gera_Tela_Quadro(Quadroci+3,Quadroli+5,Quadrocf-3,Quadrolf-1,cort,corf); //Retangulo interior em rela��o ao quadro central
	
	gotoxy(Quadroci+5,Quadroli+5); //posicionamento do texto
	textcolor(10);
	printf("Leitura");
	
	textcolor(11);
	gotoxy(25,9);printf("Cod.Assunto");
	Gera_Tela_Quadro(Quadroci+5,Quadroli+8,Quadrocf-5,Quadrolf-13,cort,corf); //Campo de digita��o do texto
	gotoxy(26,11);puts(st.codassunto);
	
	textcolor(13);
	gotoxy(36,13);printf("Descricao");
	Gera_Tela_Quadro(Quadroci+5,Quadroli+12,Quadrocf-5,Quadrolf-9,cort,corf); //Campo de digita��o do texto
	gotoxy(37,15);puts(st.descricao);

	
	gotoxy(Quadroci+5,Quadroli+21); // posicionamento das opcoes abaixo
	
	textcolor(15);
	printf("(<-)Anterior||    ||(->)Proximo");
	
	textcolor(12);
	gotoxy(Quadroci+13,Quadroli+20);printf("(ESC)-Para Sair");
	
	textcolor(14);
	//Fim tela de Leitura
	///	gotoxy(26, 11); ///////////escrita;
	
}


void Gera_Tela_Leitura_Palavra(unsigned char Quadroci,unsigned char Quadroli,unsigned char Quadrocf,unsigned char Quadrolf,unsigned char cort,unsigned char corf,ST_Palavras st){
	
	///Inicio tela de Leitura
	Gera_Tela_Quadro(Quadroci,Quadroli,Quadrocf,Quadrolf-21,cort,corf); ///barra de titulo "Leitura"
	
	gotoxy(Quadroci+17,Quadroli+1); //posicionamento do texto
	
	textcolor(15);
	printf("Leitura");	
	Gera_Tela_Quadro(Quadroci,Quadroli+3,Quadrocf,Quadrolf,cort,corf); //Quadro central
	Gera_Tela_Quadro(Quadroci+3,Quadroli+5,Quadrocf-3,Quadrolf-1,cort,corf); //Retangulo interior em rela��o ao quadro central
	
	gotoxy(Quadroci+5,Quadroli+5); //posicionamento do texto
	textcolor(10);
	printf("Leitura");
	
	textcolor(11);
	gotoxy(25,9);printf("Palavra");
	Gera_Tela_Quadro(Quadroci+5,Quadroli+8,Quadrocf-5,Quadrolf-13,cort,corf); //Campo de digita��o do texto
	gotoxy(26,11);puts(st.palavra);
	
	textcolor(10);
	gotoxy(42,13);printf("Cod.Assunto");
	Gera_Tela_Quadro(Quadroci+22,Quadroli+12,Quadrocf-4,Quadrolf-9,cort,corf); //Campo de digita��o do texto
	gotoxy(43,15);puts(st.codassunto);
	
	textcolor(10);
	gotoxy(26,13);printf("Cod.Palavra");
	Gera_Tela_Quadro(Quadroci+4,Quadroli+12,Quadrocf-20,Quadrolf-9,cort,corf); //Campo de digita��o do texto
	gotoxy(27,15);puts(st.codpalavra);
	
	textcolor(13);
	gotoxy(46,17);printf("Descricao");
	Gera_Tela_Quadro(Quadroci+26,Quadroli+16,Quadrocf-4,Quadrolf-5,cort,corf); //Campo de digita��o do texto
	gotoxy(47,19);puts(st.descricao);
	
	textcolor(13);
	gotoxy(32,17);printf("Nivel");
	Gera_Tela_Quadro(Quadroci+8,Quadroli+16,Quadrocf-20,Quadrolf-5,cort,corf); //Campo de digita��o do texto
	gotoxy(33,19);printf("%6d",st.nivel);
	
	gotoxy(Quadroci+5,Quadroli+21); // posicionamento das opcoes abaixo
	
	textcolor(15);
	printf("(<-)Anterior||    ||(->)Proximo");
	
	textcolor(12);
	gotoxy(Quadroci+13,Quadroli+20);printf("(ESC)-Para Sair");
	
	textcolor(14);
	//Fim tela de Leitura
	///	gotoxy(26, 11); ///////////escrita;
	
}


void Gera_Tela_Leitura(unsigned char Quadroci,unsigned char Quadroli,unsigned char Quadrocf,unsigned char Quadrolf,unsigned char cort,unsigned char corf,char elem[],int level){
	
	///Inicio tela de Leitura
	Gera_Tela_Quadro(Quadroci,Quadroli,Quadrocf,Quadrolf-21,cort,corf); ///barra de titulo "Leitura"
	
	gotoxy(Quadroci+17,Quadroli+1); //posicionamento do texto
	
	textcolor(15);
	printf("Leitura");	
	Gera_Tela_Quadro(Quadroci,Quadroli+3,Quadrocf,Quadrolf,cort,corf); //Quadro central
	Gera_Tela_Quadro(Quadroci+3,Quadroli+5,Quadrocf-3,Quadrolf-1,cort,corf); //Retangulo interior em rela��o ao quadro central
	
	gotoxy(Quadroci+5,Quadroli+5); //posicionamento do texto
	textcolor(10);
	printf("Leitura");
	
	textcolor(11);
	gotoxy(25,9);printf("Nome");
	Gera_Tela_Quadro(Quadroci+5,Quadroli+8,Quadrocf-5,Quadrolf-13,cort,corf); //Campo de digita��o do texto
	gotoxy(26,11);puts(elem);
	
	textcolor(13);
	gotoxy(36,13);printf("Lv");
	Gera_Tela_Quadro(Quadroci+16,Quadroli+12,Quadrocf-14,Quadrolf-9,cort,corf); //Campo de digita��o do texto
	gotoxy(37,15);printf("%d",level);

	
	gotoxy(Quadroci+5,Quadroli+21); // posicionamento das opcoes abaixo
	
	textcolor(15);
	printf("(<-)Anterior||    ||(->)Proximo");
	
	textcolor(12);
	gotoxy(Quadroci+13,Quadroli+20);printf("(ESC)-Para Sair");
	
	textcolor(14);
	//Fim tela de Leitura
	///	gotoxy(26, 11); ///////////escrita;
	
}


void Gera_Tela_Menu_Login(unsigned char Quadroci,unsigned char Quadroli,unsigned char Quadrocf,unsigned char Quadrolf,unsigned char cort,unsigned char corf){
	
	///Inicio tela de Cadastro
	Gera_Tela_Quadro(Quadroci,Quadroli,Quadrocf,Quadrolf-21,cort,corf); ///barra de titulo "Cadastrar"
	
	gotoxy(Quadroci+17,Quadroli+1); //posicionamento do texto
	textcolor(10);
	printf("Login");	
	
	Gera_Tela_Quadro(Quadroci,Quadroli+3,Quadrocf,Quadrolf,cort,corf); //Quadro central
	
	Gera_Tela_Quadro(Quadroci+3,Quadroli+5,Quadrocf-3,Quadrolf-1,cort,corf); //Retangulo interior em rela��o ao quadro central
	gotoxy(Quadroci+5,Quadroli+5); //posicionamento do texto
	textcolor(11);
	printf("Login");
	
	gotoxy(25,9);
		
	textcolor(15);
	printf("Codigo");
	
	Gera_Tela_Quadro(Quadroci+5,Quadroli+8,Quadrocf-5,Quadrolf-13,cort,corf); //Campo de digita��o do texto
	gotoxy(Quadroci+5,Quadroli+21); // posicionamento das opcoes abaixo
	
	textcolor(11);
	printf("Digite o Nome do Usuario");
	
	textcolor(14);
	//Fim tela de Cadastro
	///	gotoxy(26, 11); ///////////escrita;
	
}



void Gera_Tela_Quadro(int coli,int lini,int colf,int linf,int cort,int corf){
	
	textcolor(cort);
	textbackground(corf);
	
	gotoxy(coli,linf);printf("%c",192);
	gotoxy(colf,lini);printf("%c",191);
	gotoxy(coli,lini);printf("%c",218);
	gotoxy(colf,linf);printf("%c",217);
	
	/*
	///Preenche Quadro
	PreencheGera_Tela_Quadro(coli,colf,lini,linf);
	*/	
	
	///Gera Coluna
	
	for(int i=coli+1;i<colf;i++){
		
		gotoxy(i,lini);printf("%c",196);
		gotoxy(i,linf);printf("%c",196);
		
	}
	
	///Gera Linha
	for(int i=lini+1;i<linf;i++){
		
		gotoxy(coli,i);printf("%c",179);
		gotoxy(colf,i);printf("%c",179);
		
	}
	
}


void Gera_Tela_Menu_Alt(unsigned char Quadroci,unsigned char Quadroli,unsigned char Quadrocf,unsigned char Quadrolf,unsigned char cort,unsigned char corf){
	
	Gera_Tela_Quadro(Quadroci,Quadroli,Quadrocf,Quadrolf,cort,corf);///QuadroMaior
	
	////////////////////////////////////////////////////////////////////////////////////
	gotoxy(Quadroci+2,Quadroli+1);printf("*__Menu__*");
	
	Gera_Tela_Quadro(Quadroci+2,Quadroli+3,Quadrocf-2,Quadrolf-11,cort,corf);///QuadrosMenores
	gotoxy(Quadroci+3,Quadroli+4);printf("   Usuario ");
	
	
	
	Gera_Tela_Quadro(Quadroci+2,Quadroli+6,Quadrocf-2,Quadrolf-8,cort,corf);///QuadrosMenores
	gotoxy(Quadroci+4,Quadroli+7);printf("  Palavas ");
	
	
	Gera_Tela_Quadro(Quadroci+2,Quadroli+9,Quadrocf-2,Quadrolf-5,cort,corf);///QuadrosMenores
	gotoxy(Quadroci+4,Quadroli+10);printf("  Assunto");
	
	
	Gera_Tela_Quadro(Quadroci+2,Quadroli+12,Quadrocf-2,Quadrolf-2,cort,corf);///QuadrosMenores
	gotoxy(Quadroci+4,Quadroli+13);printf("   Sair  ");
	
	
	
	gotoxy(Quadroci+6,Quadroli+18);
	/////////////////////////////////////////////////////////////////////////////////////
	
}

void Gera_Tela_Intermediaria_Menu_Alt(FILE*arq,int opc){
	
	char tecla,i=6;
	
	Func_Muda_Tela();
	Gera_Tela_Menu_Alt(34,2,50,18,14,0);
	
	textcolor(15);
	
	Func_Controla_Menu(i,34,2);
	
	do{
		
		while(!kbhit()){
			Gera_Tela_Piscante(34,2,50,18,14,0);
		}
		tecla=getch();
		if(tecla==-32||tecla==0){
			tecla=getch();
		}
		
		if(tecla==13){
			if(i==6){
				if(opc==13){
					Alt_Cadastra(arq,1);
				}else
				if(opc==14){
					Mod_Exibe(arq,1);
				}
					
			}else
			if(i==7){
				if(opc==13){
					Alt_Cadastra(arq,2);
				}else
				if(opc==14){
					Mod_Exibe(arq,2);
				}
			
			}else
			if(i==8){
				if(opc==13){
					Alt_Cadastra(arq,3);
				}else
				if(opc==14){
					Mod_Exibe(arq,3);
				}
			
			}else{
				tecla=27;
			}
			clrscr();	
		}else		
		if(tecla==80){
			i++;
			if(i>9){
				i=6;
			}
			
		}else
		if(tecla==72){
			i--;
			if(i<6){
				i=9;
			}
			
		}
		
		Func_Muda_Tela();
		Gera_Tela_Menu_Alt(34,2,50,18,14,0);
		
		textcolor(15);
		Func_Controla_Menu(i,34,2);
	
	}while(tecla!=27);
	
}

void Func_Controla_Menu(char i,unsigned char Quadroci,unsigned char Quadroli){
	
	textbackground(4);
	
	switch(i){
	
		case 0:
			
			gotoxy(Quadroci+3,Quadroli+4);
			printf(" Cadastro  ");
			
		break;
		
		case 1:
			
			gotoxy(Quadroci+4,Quadroli+7);
			printf("  Exibe  ");
			
		break;
		
		case 2:
			
			gotoxy(Quadroci+4,Quadroli+10);
			printf("  Logar  ");
			
		break;
		
		case 3:
			
			gotoxy(Quadroci+3,Quadroli+13);
			printf("  Ranking ");
			
		break;
		
		case 4:
			
			gotoxy(Quadroci+3,Quadroli+2);
			printf("             Iniciar Jogo             ");
	
		break;
		
		case 5:
			
			gotoxy(Quadroci+3,Quadroli+5);
			printf("                Salvar                ");
			
		break;
		
		case 6:
			
			gotoxy(Quadroci+3,Quadroli+4);
			printf("  Usuario  ");
			
		break;
		
		case 7:
			
			gotoxy(Quadroci+4,Quadroli+7);
			printf(" Palavas  ");	
			
		break;
		
		case 8:
			
			gotoxy(Quadroci+4,Quadroli+10);
			printf(" Assunto ");
			
		break;
		
		case 9:
			
			gotoxy(Quadroci+4,Quadroli+13);
			printf("  Sair   ");
			
		break;
		
		case 10:
			
			gotoxy(Quadroci+3,Quadroli+2);
			printf("            Exclusao Fisica           ");
			
		break;
		
		case 11:
			
			gotoxy(Quadroci+3,Quadroli+5);
			printf("            Exclusao Logica           ");
			
		break;
		
		case 13:
			
			gotoxy(Quadroci+3,Quadroli+4);
			printf("  Cadastro ");
	
		break;
		
		case 14:
			
			gotoxy(Quadroci+3,Quadroli+7);
			printf(" relatorio ");
	
		break;
		
		case 15:
			
			gotoxy(Quadroci+3,Quadroli+10);
			printf(" varredura ");
	
		break;
		
		case 16:
			
			gotoxy(Quadroci+4,Quadroli+13);
			printf("   Sair  ");
	
		break;
		
		case 17:
			
			gotoxy(Quadroci+3,Quadroli+8);
			printf("                 Sair                 ");
	
		break;
		
		case 18:
			
			gotoxy(Quadroci+4,Quadroli+16);
			printf("  Sair  ");
			
		break;
		
	}
	
}

void Alt_Cadastra(FILE*arq,int param){
	
	char tecla;
	do{
		
		Func_Muda_Tela();
		Gera_Tela_Cadastro(20,2,60,25,14,0);
		
		while(!kbhit()){
			Gera_Tela_Piscante(20,2,60,25-21,14,0);
		}
		
		tecla=getch();
		if(tecla==-32||tecla==0){
			tecla=getch();
		}
		
		switch(tecla){
			
			case 82:///ins
			
				Mod_Inserir(arq,param);
				
			break;
			
			case 83:///del
		
				Mod_Deleta(arq,param);
				
			break;
			
			case 60:///alt
		
				Mod_Altera(arq,param);
		
			break;
			
		}
		
	}while(tecla!=27);
	
}

void Gera_Tela_Cadastro(unsigned char Quadroci,unsigned char Quadroli,unsigned char Quadrocf,unsigned char Quadrolf,unsigned char cort,unsigned char corf){
	
	///Inicio tela de Cadastro
	Gera_Tela_Quadro(Quadroci,Quadroli,Quadrocf,Quadrolf-21,cort,corf); ///barra de titulo "Cadastrar"
	gotoxy(Quadroci+17,Quadroli+1); //posicionamento do texto
	
	textcolor(10);
	printf("Cadastrar");	
	
	Gera_Tela_Quadro(Quadroci,Quadroli+3,Quadrocf,Quadrolf,cort,corf); //Quadro central
	
	Gera_Tela_Quadro(Quadroci+3,Quadroli+5,Quadrocf-3,Quadrolf-1,cort,corf); //Retangulo interior em rela��o ao quadro central
	gotoxy(Quadroci+5,Quadroli+5); //posicionamento do texto
	
	textcolor(11);
	printf("Cadastro");
	
	gotoxy(25,9);
	
	textcolor(11);	
	printf("Texto");
	
	Gera_Tela_Quadro(Quadroci+5,Quadroli+8,Quadrocf-5,Quadrolf-13,cort,corf); //Campo de digita��o do texto
	gotoxy(Quadroci+4,Quadroli+21); // posicionamento das opcoes abaixo
	
	
	textcolor(15);
	printf("(Ins)erir|(F2)Altera|(Del)Excluir");
	
	gotoxy(Quadroci+13,Quadroli+20);
	
	textcolor(12);
	printf("(ESC)-Para Sair");
	
	textcolor(14);
	
	//Fim tela de Cadastro
	///	gotoxy(26, 11); ///////////escrita;
	
}
void Gera_Tela_Menu(unsigned char Quadroci,unsigned char Quadroli,unsigned char Quadrocf,unsigned char Quadrolf,unsigned char cort,unsigned char corf){
	
	Gera_Tela_Quadro(Quadroci,Quadroli,Quadrocf,Quadrolf+2,cort,corf);///QuadroMaior
	
	////////////////////////////////////////////////////////////////////////////////////
	gotoxy(Quadroci+2,Quadroli+1);printf(" *__Menu__* ");
	
	Gera_Tela_Quadro(Quadroci+2,Quadroli+3,Quadrocf-2,Quadrolf-11,cort,corf);///QuadrosMenores
	gotoxy(Quadroci+3,Quadroli+4);printf("  Cadastro ");
	
	
	Gera_Tela_Quadro(Quadroci+2,Quadroli+6,Quadrocf-2,Quadrolf-8,cort,corf);///QuadrosMenores
	gotoxy(Quadroci+4,Quadroli+7);printf("   Exibe  ");
	
	
	Gera_Tela_Quadro(Quadroci+2,Quadroli+9,Quadrocf-2,Quadrolf-5,cort,corf);///QuadrosMenores
	gotoxy(Quadroci+4,Quadroli+10);printf("   Logar  ");
	
	
	Gera_Tela_Quadro(Quadroci+2,Quadroli+12,Quadrocf-2,Quadrolf-2,cort,corf);///QuadrosMenores
	gotoxy(Quadroci+4,Quadroli+13);printf("  Ranking");
	
	Gera_Tela_Quadro(Quadroci+2,Quadroli+15,Quadrocf-2,Quadrolf+1,cort,corf);///QuadrosMenores
	gotoxy(Quadroci+4,Quadroli+16);printf("   Sair  ");
	
	gotoxy(Quadroci+6,Quadroli+18);
	/////////////////////////////////////////////////////////////////////////////////////
	
}

void Main_Menu(void){
	
	char tecla;
	int i=0,tl=-1;
	FILE*arq;
	
	Gera_Tela_Menu(34,2,50,18,14,0);
	textcolor(15);
	textbackground(0);
	
	gotoxy(34+3,2+4);
	printf(" Cadastro  ");
	Func_Controla_Menu(i,34,2);
	
	//estrutura ex;
	//	Func_Inicializa_Estrutura(ex);
	
	
	do{
		
		while(!kbhit()){
			Gera_Tela_Piscante(34,2,50,20,14,0);
		}
		///clrscr();
		tecla=getch();
		if(tecla==-32||tecla==0){
			tecla=getch();
		}
		
		if(tecla==13){
			Alt_Menu(i,tecla,arq);
			clrscr();
		}else		
		if(tecla==80){
			i++;
			if(i>18){
				i=0;
			}else
			if(i>3){
				i=18;
			}
			
		}else
		if(tecla==72){
			i--;
			if(i<0){
				i=18;
			}else
			if(i>3){
				i=3;
			}
			
		}
		
		Func_Muda_Tela();
		Gera_Tela_Menu(34,2,50,18,14,0);
		
		textcolor(15);
		Func_Controla_Menu(i,34,2);

	}while(tecla!=27);
	
	gotoxy(2,22);
	
}

void Alt_Menu(int i,char &tecla,FILE*arq){
	Func_Muda_Tela();
	switch(i){
			
		case 0:///cadastra
		
				
				Func_Inserir_Usuario(arq,1);
					
		break;
				
		case 1:///exibe
		
				Alt_Exibe_usuario(arq,Func_Tam(1));
				
		break;
				
		case 2:///Logar
		
				Login(arq);
				
		break;
				
		case 3:///Exibe Ranking
		
				Mod_Exibe(arq,4);
				
		break;
		
		case 18:///sair
		
				tecla=27;
				
		break;
		
			
	}
			
}

void Menu_Adm(FILE*arq){
	char tecla;
	int i=13;
	
	Func_Muda_Tela();
	Gera_Tela_Menu_ADM(34,2,50,18,14,0);
	textcolor(15);
	textbackground(0);
	
	Func_Controla_Menu(i,34,2);
	do{
	
		while(!kbhit()){
			Gera_Tela_Piscante(34,2,50,18,14,0);
		}
		tecla=getch();
		if(tecla==-32||tecla==0){
			tecla=getch();
		}
		
		if(tecla==13){
			if(i>12&&i<15){
				Gera_Tela_Intermediaria_Menu_Alt(arq,i);
			}else
			if(i==15){
				Gera_Mod_Deleta_Fisica_Geral(arq);
			}else{
				tecla=27;
			}
			clrscr();
		}else		
		if(tecla==80){
			i++;
			if(i>16){
				i=13;
			}
			
		}else
		if(tecla==72){
			i--;
			if(i<13){
				i=16;
			}
			
		}
		
		Func_Muda_Tela();
		Gera_Tela_Menu_ADM(34,2,50,18,14,0);
		
		textcolor(15);
		Func_Controla_Menu(i,34,2);

	}while(tecla!=27);
	
	gotoxy(2,22);
}
void Gera_Tela_Piscante(unsigned char Quadroci,unsigned char Quadroli,unsigned char Quadrocf,unsigned char Quadrolf,unsigned char cort,unsigned char corf){
	/*int i,k;
	i=rand()%18+1;
	k=rand()%8+1;
	Gera_Tela_Quadro(Quadroci,Quadroli,Quadrocf,Quadrolf,i,k);
	textcolor(cort);
	textbackground(corf);
	_sleep(180);*/
	
}




	

void Gera_Tela_Menu_ADM(unsigned char Quadroci,unsigned char Quadroli,unsigned char Quadrocf,unsigned char Quadrolf,unsigned char cort,unsigned char corf){
	
	Gera_Tela_Quadro(Quadroci,Quadroli,Quadrocf,Quadrolf,cort,corf);///QuadroMaior
	
	////////////////////////////////////////////////////////////////////////////////////
	gotoxy(Quadroci+2,Quadroli+1);printf(" *__Menu__* ");
	
	Gera_Tela_Quadro(Quadroci+2,Quadroli+3,Quadrocf-2,Quadrolf-11,cort,corf);///QuadrosMenores
	gotoxy(Quadroci+3,Quadroli+4);printf("  Cadastro ");
	
	
	Gera_Tela_Quadro(Quadroci+2,Quadroli+6,Quadrocf-2,Quadrolf-8,cort,corf);///QuadrosMenores
	gotoxy(Quadroci+3,Quadroli+7);printf(" relatorio ");
	
	
	Gera_Tela_Quadro(Quadroci+2,Quadroli+9,Quadrocf-2,Quadrolf-5,cort,corf);///QuadrosMenores
	gotoxy(Quadroci+3,Quadroli+10);printf(" varredura ");
	
	
	Gera_Tela_Quadro(Quadroci+2,Quadroli+12,Quadrocf-2,Quadrolf-2,cort,corf);///QuadrosMenores
	gotoxy(Quadroci+4,Quadroli+13);printf("   Sair  ");
	
	gotoxy(Quadroci+6,Quadroli+18);
	/////////////////////////////////////////////////////////////////////////////////////
	
}

void Gera_Mod_Deleta_Fisica_Geral(FILE*arq){
	char tecla,i=6;
	
	Func_Muda_Tela();
	Gera_Tela_Menu_Alt(34,2,50,18,14,0);
	
	textcolor(15);
	
	Func_Controla_Menu(i,34,2);
	
	do{
	
	    while(!kbhit()){
			Gera_Tela_Piscante(34,2,50,18,14,0);
		}
		tecla=getch();
		if(tecla==-32||tecla==0){
			tecla=getch();
		}
		
		if(tecla==13){
			
			if(i>5&&i<9){
				Mod_Deleta_Fisica_Geral(arq,i);
				tecla=27;
			}else{
				tecla=27;
			}
			
			clrscr();	
			
		}else		
		if(tecla==80){
			i++;
			if(i>9){
				i=6;
			}
			
		}else
		if(tecla==72){
			i--;
			if(i<6){
				i=9;
			}
			
		}
		
		Func_Muda_Tela();
		Gera_Tela_Menu_Alt(34,2,50,18,14,0);
		
		textcolor(15);
		Func_Controla_Menu(i,34,2);
	
	}while(tecla!=27);
	
}

void Mod_Deleta(FILE*arq,int param){
	char aux[TAM_NOME];
	
	Func_Muda_Tela();
	Gera_Tela_Cadastro(20,2,60,25,14,0);
	
	gotoxy(25,9);
	printf("__*Remover*__");
	
	gotoxy(26, 11);
	gets(aux);
	
	Func_Muda_Tela();
	char tecla;
	int i=10;
	
	Gera_Tela_Menu_Exclusao(12,7,55,15,14,0);
	textcolor(15);
	textbackground(0);
	
	Func_Controla_Menu(i,12,7);
	do{
	
		tecla=getch();
		if(tecla==-32||tecla==0){
			tecla=getch();
		}
		
		if(tecla==13){
			if(i==10){
				Mod_Deleta_Fisica(arq,aux,param);
			}else
			if(i==11){
				Mod_Deleta_Logica(arq,aux,param);
			}
			tecla=27;
			clrscr();
		}else		
		if(tecla==80){
			i++;
			if(i>11){
				i=10;
			}
			
		}else
		if(tecla==72){
			i--;
			if(i<10){
				i=11;
			}
			
		}
		
		Func_Muda_Tela();
		Gera_Tela_Menu_Exclusao(12,7,55,15,14,0);
		
		textcolor(15);
		Func_Controla_Menu(i,12,7);

	}while(tecla!=27);
	
	gotoxy(2,22);
	
	
}

void Gera_Tela_Menu_Exclusao(unsigned char Quadroci,unsigned char Quadroli,unsigned char Quadrocf,unsigned char Quadrolf,unsigned char cort,unsigned char corf){

	Gera_Tela_Quadro(Quadroci,Quadroli,Quadrocf,Quadrolf,cort,corf);///QuadroMaior
	
	////////////////////////////////////////////////////////////////////////////////////
	Gera_Tela_Quadro(Quadroci+2,Quadroli+1,Quadrocf-2,Quadrolf-5,cort,corf);///QuadrosMenores
	gotoxy(Quadroci+3,Quadroli+2);printf("             Exclusao Fisica          ");
	
	Gera_Tela_Quadro(Quadroci+2,Quadroli+4,Quadrocf-2,Quadrolf-2,cort,corf);///QuadrosMenores
	gotoxy(Quadroci+3,Quadroli+5);printf("             Exclusao Logica          ");
	
	gotoxy(Quadroci+6,Quadroli+18);
	/////////////////////////////////////////////////////////////////////////////////////
	
}

void Login(FILE*arq){
	
	char nome[TAM_NOME];	///tela de leitura de usuario
	int pos;
	ST_usuarios st;
	
	Func_Muda_Tela();
	Gera_Tela_Menu_Login(20,2,60,25,14,0);
	
	gotoxy(26, 11);
	gets(nome);
	
	arq=fopen("Arquivo_usuarios.dat","rb");
	if(arq!=NULL){
		pos=Busca_Binario_usuario(Func_Tam(1),nome);
		
		fseek(arq,sizeof(ST_usuarios)*pos,0);
		fread(&st,sizeof(ST_usuarios),1,arq);
		
		fclose(arq);
		if(Func_Tam(1)==-1){
		
			gotoxy(26, 11);
			
			textcolor(12);
			printf("Nenhuma Palavra Cadastrada");
			textcolor(14);
			_sleep(500);
		
		}else
		if(pos==-1||st.status==0){
			
			gotoxy(26, 11);
			
			textcolor(12);
			printf("login inexistente");
			textcolor(14);
			_sleep(500);
			
			
		}else
		if(st.tipo==1){
			Menu_Adm(arq);
		}else{
			Gera_Tela_Intermediaria_Jogo(arq,st,pos);
		}
		
	}

}





///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void Mod_Inserir(FILE*arq,int param){
	
	Func_Muda_Tela();
	Gera_Tela_Cadastro(20,2,60,25,14,0);
	
	textcolor(11);
	gotoxy(25,7);
	printf("*Codigo*");
	textcolor(14);
	
	switch(param){
		case 1:
			Func_Inserir_Usuario(arq,param);
		break;
		
		case 2:
			Func_Inserir_Palavra(arq,param);
		break;
		
		case 3:
			Func_Inserir_Assunto(arq,param);
		break;
	}
	
}			


void Func_Inserir_Usuario(FILE*arq,int param){
	
	Gera_Tela_Cadastro(20,2,60,25,14,0);
	
	textcolor(11);
	gotoxy(25,7);
	printf("*Inserir*");
	textcolor(14);
	gotoxy(25,9);
	textcolor(15);
	printf("*Inserir Codigo*");
	textcolor(14);
	
	char chave[TAM_NOME];
	gotoxy(26, 11);
	gets(chave);
	
	int pos=Busca_Binario_usuario(Func_Tam(param),chave);
	if(pos==-1){
			Func_Digita_usuario(arq,chave);
			Ordenacao_Direta_usuario(Func_Tam(param));
	}else{
		gotoxy(26, 11);
		
		textcolor(12);
		printf("J� Cadastrado");
		textcolor(14);
		
		_sleep(500);
	}
}

void Func_Digita_usuario(FILE*arqDIG,char chave[]){
	
	arqDIG=fopen("Arquivo_usuarios.dat","ab+");///
	
	if(arqDIG!=NULL){
		
		ST_usuarios st;
		
		gotoxy(25,9);
		printf("__*Login*__");	
		gotoxy(26, 11);
		
		
		 Func_Escreve_usuario(st,chave);
	
		fwrite(&st,sizeof(ST_usuarios),1,arqDIG);
	
		fclose(arqDIG);
		
	}else{
		
		gotoxy(26, 11);
		
		textcolor(12);
		printf("Inexistente");
		textcolor(14);
		
		_sleep(500);
		
	}
	
}

void Func_Escreve_usuario(ST_usuarios &st,char chave[]){
	
	
	strcpy(st.login,chave);
	
	Func_Muda_Tela();
	Gera_Tela_Cadastro(20,2,60,25,14,0);
	
	gotoxy(25,9);
	printf("__*Nome*__");
	gotoxy(26, 11);
	gets(st.nome);
	
	st.ponttotal=0;
	st.status=1;
	st.tipo=0;
	st.level=0;
			
}

void Func_Inserir_Palavra(FILE*arq,int param){
	
	char chave[TAM_NOME];
	gotoxy(26, 11);
	gets(chave);
	
	int pos=Busca_Sentinela_Palavra(Func_Tam(param),chave);
	if(pos==-1){
			Func_Digita_Palavra(arq,chave);
			OrdenaBolha_Palavra(Func_Tam(param));
	}else{
		gotoxy(26, 11);
		
		textcolor(12);
		printf("J� Cadastrado");
		textcolor(14);
		
		_sleep(500);
	}
}

void Func_Digita_Palavra(FILE*arqDIG,char chave[]){
	
	arqDIG=fopen("Arquivo_Palavras.dat","ab+");
	
	if(arqDIG!=NULL){
		
		ST_Palavras st;
		char assunto[TAM_COD];
		
		gotoxy(25,9);
		printf("__*Codigo Do Palavra*__");	
		gotoxy(26, 11);
		
		Func_Muda_Tela();
		Gera_Tela_Cadastro(20,2,60,25,14,0);
		
		gotoxy(25,9);
		printf("__*Codigo Do Assunto*__");
		gotoxy(26, 11);
		gets(assunto);
		if(Busca_Exaustiva_Assunto(Func_Tam(3),assunto)!=-1){
			Func_Escreve_Palavra(st,chave,assunto);
		
			fwrite(&st,sizeof(ST_Palavras),1,arqDIG);
		}else{
			gotoxy(26, 11);
		
			textcolor(12);
			printf("Asssunto Inexistente");
			textcolor(14);
			
			_sleep(500);
		}
			 
	
		fclose(arqDIG);
		
	}else{
		
		gotoxy(26, 11);
		
		textcolor(12);
		printf("Inexistente");
		textcolor(14);
		
		_sleep(500);
		
	}
	
}


void Func_Escreve_Palavra(ST_Palavras &st,char chave[],char assunto[]){
	
	
	strcpy(st.codpalavra,chave);
	strcpy(st.codassunto,assunto);
	st.status=1;
	
	Func_Muda_Tela();
	Gera_Tela_Cadastro(20,2,60,25,14,0);
	
	gotoxy(25,9);
	printf("__*Palavra*__");
	gotoxy(26, 11);
	gets(st.palavra);
	
	st.pontuacao=0;
	st.nivel=0;
	st.status=1;
		
}

void Func_Inserir_Assunto(FILE*arq,int param){
	
	char chave[TAM_NOME];
	gotoxy(26, 11);
	gets(chave);
	
	int pos=Busca_Exaustiva_Assunto(Func_Tam(param),chave);
	if(pos==-1){
			Func_Digita_Assunto(arq,chave);
			OrdenaBolha_Assunto(Func_Tam(param));
	}else{
		gotoxy(26, 11);
		
		textcolor(12);
		printf("J� Cadastrado");
		textcolor(14);
		
		_sleep(500);
	}
}

void Func_Digita_Assunto(FILE*arqDIG,char chave[]){
	
	arqDIG=fopen("Arquivo_Assuntos.dat","ab+");
	
	if(arqDIG!=NULL){
		
		ST_Assuntos st;
		
		gotoxy(25,9);
		printf("__*Codigo do Assunto*__");	
		gotoxy(26, 11);
		
		
		 Func_Escreve_Assunto(st,chave);
	
		fwrite(&st,sizeof(ST_Assuntos),1,arqDIG);
	
		fclose(arqDIG);
		
	}else{
		
		gotoxy(26, 11);
		
		textcolor(12);
		printf("Inexistente");
		textcolor(14);
		
		_sleep(500);
		
	}
	
}

void Func_Escreve_Assunto(ST_Assuntos &st,char chave[]){
	
	
	strcpy(st.codassunto,chave);
	
	Func_Muda_Tela();
	Gera_Tela_Cadastro(20,2,60,25,14,0);
	
	gotoxy(25,9);
	printf("__*Descricao*__");
	gotoxy(26, 11);
	gets(st.descricao);
	
	st.status=1;
		
}

int Func_Tam(int a){
	FILE*arqtam;
	
	int aux;

	switch(a){///arquivos
		case 1:
			
			arqtam=fopen("Arquivo_usuarios.dat","ab");
			if(arqtam==NULL){
				aux=1;
			}else{
				fseek(arqtam, 0, SEEK_END);
				aux=ftell(arqtam)/sizeof(ST_usuarios);
			}
			
		break;
		
		case 2:
			
			arqtam=fopen("Arquivo_Palavras.dat","ab");
			if(arqtam==NULL){
				aux=1;
			}else{
				fseek(arqtam, 0, SEEK_END);
				aux=ftell(arqtam)/sizeof(ST_Palavras);
			}
			
		break;
		
		case 3:
			
			arqtam=fopen("Arquivo_Assuntos.dat","ab");
			if(arqtam==NULL){
				aux=1;
			}else{
				fseek(arqtam, 0, SEEK_END);
				aux=ftell(arqtam)/sizeof(ST_Assuntos);
			}
			
		break;
		
		case 4:
			
			arqtam=fopen("Arquivo_Ranking.dat","ab");
			if(arqtam==NULL){
				aux=1;
			}else{
				fseek(arqtam, 0, SEEK_END);
				aux=ftell(arqtam)/sizeof(ST_Ranking);
			}
			
		break;
			
	}

	fclose(arqtam);
	return aux;
}

char Busca_Sentinela(int Tam,char chave[]){///SEMPRE DEVE HAVER UMA POSICAO AMAIS NA DECLARACAO; ST[TAM+1];
	ST_Palavras st[Tam+1];
	char i=0;
	
	FILE*arqBS=fopen("Arquivo_Palavras.dat","rb");
	fread(&st,sizeof(ST_Palavras),Tam,arqBS);
	fclose(arqBS);
	
	/*
	Exibir_Mensagens(8);
	fflush(stdin);
	gets(chave);
	*/
	
	strcpy(st[Tam].codpalavra,chave) ;
	while(stricmp(chave,st[i].codpalavra)!=0)
		i++;
	///if(i<Tam&&stricmp(chave,st[i].c)==0)
	if(i<Tam)
		return i;
	else
		return -1;
		
/////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////
}


void Mod_Altera(FILE*arq,int param){
	
	char chave[TAM_NOME];
	
	Func_Muda_Tela();
	Gera_Tela_Cadastro(20,2,60,25,14,0);
	
	gotoxy(25,9);
	printf("__*Altera*__");
	
	switch(param){
		
		case 1:
			Func_Altera_usuario(arq,param);
		break;
		
		case 2:
			Func_Altera_Palavra(arq,param);
		break;
		
		case 3:
			Func_Altera_Assunto(arq,param);
		break;
	
	}

}


void Func_Altera_Palavra(FILE*arq,int param){
	
	char chave[TAM_NOME];
	gotoxy(26, 11);
	gets(chave);
	
	int pos=Busca_Sentinela_Palavra(Func_Tam(param),chave);	
	//if(Func_Validacao(param,Pos,2)==1){
	
	if(pos!=-1){
		Func_Modifica_Palavra(arq,pos);///Func_Modifica_Pessoas(arq,aux);
		OrdenaBolha_Palavra(Func_Tam(param));
	}else{
		gotoxy(26, 11);
		
		textcolor(12);
		printf("Inexistente");
		textcolor(14);
		
		_sleep(500);
	}
	
}


void Func_Modifica_Palavra(FILE*arqALT,int pos){
	arqALT=fopen("Arquivo_Palavras.dat","rb+");
	
	if(arqALT!=NULL){	
	
		char chave[TAM_NOME];
		ST_Palavras st;
		char assunto[TAM_COD];
		
		gotoxy(25,9);
		printf("__*Novo Codigo*__");
		gotoxy(26, 11);
		
		fflush(stdin);
		gets(chave);
		
		Func_Muda_Tela();
		Gera_Tela_Cadastro(20,2,60,25,14,0);
		
		gotoxy(25,9);
		printf("__*Codigo Do Assunto*__");
		gotoxy(26, 11);
		gets(assunto);
		
		if(Busca_Exaustiva_Assunto(Func_Tam(3),assunto)!=-1){
			Func_Escreve_Palavra(st,chave,assunto);
			 
			fseek(arqALT,(pos*sizeof(ST_Palavras)),0);
			
			fwrite(&st,sizeof(ST_Palavras),1,arqALT);
		}else{
			gotoxy(26, 11);
		
			textcolor(12);
			printf("Asssunto Inexistente");
			textcolor(14);
			
			_sleep(500);
		}
	
		fclose(arqALT);
		
	}else{
	
		gotoxy(26, 11);
		
		textcolor(12);
		printf("Arquivo Inexistente");
		textcolor(14);
		
		_sleep(500);
		
	}
}

void Func_Altera_usuario(FILE*arq,int param){
	
	char chave[TAM_NOME];
	gotoxy(26, 11);
	gets(chave);
	
	int pos=Busca_Binario_usuario(Func_Tam(param),chave);	
	//if(Func_Validacao(param,Pos,2)==1){
	
	if(pos!=-1){
		Func_Modifica_usuario(arq,pos);///Func_Modifica_Pessoas(arq,aux);
		Ordenacao_Direta_usuario(Func_Tam(param));
	}else{
		gotoxy(26, 11);
		
		textcolor(12);
		printf("Inexistente");
		textcolor(14);
		
		_sleep(500);
	}
	
}

void Func_Modifica_usuario(FILE*arqALT,int pos){
	arqALT=fopen("Arquivo_usuarios.dat","rb+");
	
	if(arqALT!=NULL){	
	
		ST_usuarios st;
		char chave[TAM_NOME];
		
		gotoxy(25,9);
		printf("__*usuario*__");
		gotoxy(26, 11);
		
		fflush(stdin);
		gets(chave);
		
		Func_Escreve_usuario(st,chave);
		 
		fseek(arqALT,(pos*sizeof(ST_usuarios)),0);
		
		fwrite(&st,sizeof(ST_usuarios),1,arqALT);
	
		fclose(arqALT);
		
	}else{
	
		gotoxy(26, 11);
		
		textcolor(12);
		printf("Arquivo Inexistente");
		textcolor(14);
		
		_sleep(500);
		
	}
}

void Func_Altera_Assunto(FILE*arq,int param){
	
	char chave[TAM_NOME];
	gotoxy(26, 11);
	gets(chave);
	
	int pos=Busca_Exaustiva_Assunto(Func_Tam(param),chave);	
	//if(Func_Validacao(param,Pos,2)==1){
	
	if(pos!=-1){
		Func_Modifica_Assunto(arq,pos);///Func_Modifica_Pessoas(arq,aux);
		OrdenaBolha_Assunto(Func_Tam(param));
	}else{
		gotoxy(26, 11);
		
		textcolor(12);
		printf("Inexistente");
		textcolor(14);
		
		_sleep(500);
	}
	
}

void Func_Modifica_Assunto(FILE*arqALT,int pos){
	arqALT=fopen("Arquivo_Assuntos.dat","rb+");
	
	if(arqALT!=NULL){	
	
		ST_Assuntos st;
		char chave[TAM_NOME];
		
		gotoxy(25,9);
		printf("__*usuario*__");
		gotoxy(26, 11);
		
		fflush(stdin);
		gets(chave);
		
		Func_Escreve_Assunto(st,chave);
		 
		fseek(arqALT,(pos*sizeof(ST_Assuntos)),0);
		
		fwrite(&st,sizeof(ST_Assuntos),1,arqALT);
	
		fclose(arqALT);
		
	}else{
	
		gotoxy(26, 11);
		
		textcolor(12);
		printf("Arquivo	Inexistente");
		textcolor(14);
		
		_sleep(500);
		
	}
}



void Mod_Deleta_Fisica(FILE*arq,char aux[],int param){
	
	Func_Muda_Tela();
	Gera_Tela_Cadastro(20,2,60,25,14,0);
	gotoxy(25,9);
	printf("__*Remover*__");
	
	switch(param){
	
		case 1:
			Func_Retira_usuario_Fisica(arq,aux);
		break;
		
		case 2:
			Func_Retira_Palavra_Fisica(arq,aux);
		break;
		
		case 3:
			if(Busca_Sentinela_Palavra_Assunto(Func_Tam(2),aux)==-1)
				Func_Retira_Assunto_Fisica(arq,aux);
			else{
				
				gotoxy(26, 11);
	
				textcolor(12);
				printf("O Assunto Possui Dependencias");
				textcolor(14);
				
				_sleep(800);
		
			}
		break;
		
	}
	
}

void Func_Retira_Palavra_Fisica(FILE*arqEXC,char chave[]){
	
	arqEXC = fopen( "Arquivo_Palavras.dat" , "rb" );
	if(arqEXC!=NULL){
		FILE*arqTemp = fopen( "Temp_Palavras.dat" , "wb+" );
		
		ST_Palavras st;
	
		fread(&st,sizeof(ST_Palavras),1,arqEXC);
	
		while(!feof(arqEXC)){
			
			if(stricmp(chave,st.codpalavra)!=0){
				fwrite(&st,sizeof(ST_Palavras),1,arqTemp);
			}
			fread(&st,sizeof(ST_Palavras),1,arqEXC);
			
		}
	
		fclose(arqTemp);
		fclose(arqEXC);
		
		remove("Arquivo_Palavras.dat");
		rename("Temp_Palavras.dat","Arquivo_Palavras.dat");
	}else{
		gotoxy(26, 11);
		
		textcolor(12);
		printf("Arquivo Inexistente");
		textcolor(14);
		
		_sleep(500);
	}
	
}

void Func_Retira_usuario_Fisica(FILE*arqEXC,char chave[]){
	
	arqEXC = fopen( "Arquivo_usuarios.dat" , "rb" );
	if(arqEXC!=NULL){
		FILE*arqTemp = fopen( "Temp_usuarios.dat" , "wb+" );
		ST_usuarios st;
	
		fread(&st,sizeof(ST_usuarios),1,arqEXC);
	
		while(!feof(arqEXC)){
			
			if(stricmp(chave,st.login)!=0){
				fwrite(&st,sizeof(ST_usuarios),1,arqTemp);
			}
			fread(&st,sizeof(ST_usuarios),1,arqEXC);
			
		}
	
		fclose(arqTemp);
		fclose(arqEXC);
		
		remove("Arquivo_usuarios.dat");
		rename("Temp_usuarios.dat","Arquivo_usuarios.dat");
	}else{
		gotoxy(26, 11);
		
		textcolor(12);
		printf("Arquivo Inexistente");
		textcolor(14);
		
		_sleep(500);
	}
	
}


void Func_Retira_Assunto_Fisica(FILE*arqEXC,char chave[]){
	
	arqEXC = fopen( "Arquivo_Assuntos.dat" , "rb" );
	if(arqEXC!=NULL){
		FILE*arqTemp = fopen( "Temp_Assuntos.dat" , "wb+" );
		ST_Assuntos st;
	
		fread(&st,sizeof(ST_Assuntos),1,arqEXC);
	
		while(!feof(arqEXC)){
			
			
			if(stricmp(chave,st.codassunto)!=0){
				fwrite(&st,sizeof(ST_Assuntos),1,arqTemp);
			}
			fread(&st,sizeof(ST_Assuntos),1,arqEXC);
			
		}
	
		fclose(arqTemp);
		fclose(arqEXC);
		
		remove("Arquivo_Assuntos.dat");
		rename("Temp_Assuntos.dat","Arquivo_Assuntos.dat");
	}else{
		gotoxy(26, 11);
		
		textcolor(12);
		printf("Arquivo Inexistente");
		textcolor(14);
		
		_sleep(500);
	}
	
} 

void Mod_Exibe(FILE*arq,int param){
	
	int Tam=Func_Tam(param);
	if(Tam>0){
		
		switch(param){
			
			case 1:
				    Alt_Exibe_usuario(arq,Tam);
			break;
			
			case 2:
				    Alt_Exibe_Palavras(arq,Tam);
			break;
			
			case 3:
				    Alt_Exibe_Assunto(arq,Tam);
			break;
			
			case 4:
					Alt_Exibe_Ranking(arq,Tam);
			break;
			
		}
		
	}else{
		
		Func_Muda_Tela();
		Gera_Tela_Leitura(20,2,60,25,14,0,"Nenhum Registro",0);
		_sleep(1000);
		
	}
	
	///fclose(arq);
}

void Alt_Exibe_usuario(FILE*arqLEI,int tl){
	
	arqLEI=fopen("Arquivo_usuarios.dat","rb");

	ST_usuarios ex;	
	char tecla;int i=0,comeco;
	
	fread(&ex,sizeof(ST_usuarios),1,arqLEI);
	while(ex.status==0){
		fread(&ex,sizeof(ST_usuarios),1,arqLEI);
		i++;
	}
	comeco=i;
	do{
		
		
		if(ex.status!=0){
		
			Func_Muda_Tela();
			Gera_Tela_Leitura_Usuario(20,2,60,25,14,0,ex);	
			
			while(!kbhit()){
				Gera_Tela_Piscante(20,2,60,25-21,14,0);
			}
			tecla=getch();
			if(tecla==-32||tecla==0){
				tecla=getch();
			}
			
		}else
		if(i==tl-1||i<=comeco){
			if(i<=comeco){
				i++;
			}
			tecla=getch();
			if(tecla==-32||tecla==0){
				tecla=getch();
			}
		}	
		
		if(i>0&&tecla==75){
			i--;	
		}else
		if(i<tl-1&&tecla==77){
			i++;
		}
			
		fseek(arqLEI,sizeof(ST_usuarios)*i,0);
		fread(&ex,sizeof(ST_usuarios),1,arqLEI);
		
		
	}while(tecla!=27);
	
	fclose(arqLEI);
	
}

void Alt_Exibe_Palavras(FILE*arqLEI,int tl){
	
	arqLEI=fopen("Arquivo_Palavras.dat","rb");

	ST_Palavras ex;	
	char tecla;int i=0,comeco;
	
	fread(&ex,sizeof(ST_Palavras),1,arqLEI);
	while(ex.status==0){
		fread(&ex,sizeof(ST_Palavras),1,arqLEI);
		i++;
	}
	comeco=i;
	do{
		
		
		if(ex.status!=0){
			
			Func_Muda_Tela();
			Gera_Tela_Leitura_Palavra(20,2,60,25,14,0,ex);
			while(!kbhit()){
				Gera_Tela_Piscante(20,2,60,25-21,14,0);
			}
			tecla=getch();
			if(tecla==-32||tecla==0){
				tecla=getch();
			}
		}else
		if(i==tl-1||i<=comeco){
			if(i<=comeco){
				i++;
			}
			tecla=getch();
			if(tecla==-32||tecla==0){
				tecla=getch();
			}
		}
		
		if(i>0&&tecla==75){
			i--;	
		}else
		if(i<tl-1&&tecla==77){
			i++;
		}
		
		fseek(arqLEI,sizeof(ST_Palavras)*i,0);
		fread(&ex,sizeof(ST_Palavras),1,arqLEI);
		
	}while(tecla!=27);
	
	fclose(arqLEI);
	
}

void Alt_Exibe_Assunto(FILE*arqLEI,int tl){
	
	arqLEI=fopen("Arquivo_Assuntos.dat","rb");

	ST_Assuntos ex;
	char tecla;int i=0,comeco;
	
	fread(&ex,sizeof(ST_Assuntos),1,arqLEI);
	while(ex.status==0){
		fread(&ex,sizeof(ST_Assuntos),1,arqLEI);
		i++;
	}
	comeco=i;
	do{
		
	
		if(ex.status!=0){
			
			Func_Muda_Tela();
			Gera_Tela_Assunto(20,2,60,25,14,0,ex);
			while(!kbhit()){
				Gera_Tela_Piscante(20,2,60,25-21,14,0);
			}
			tecla=getch();
			if(tecla==-32||tecla==0){
				tecla=getch();
			}
		}else
		if(i==tl-1||i<=comeco){
			if(i<=comeco){
				i++;
			}
			tecla=getch();
			if(tecla==-32||tecla==0){
				tecla=getch();
			}
		}
			
		if(i>0&&tecla==75){
			i--;	
		}else
		if(i<tl-1&&tecla==77){
			i++;
		}
		
		fseek(arqLEI,sizeof(ST_Assuntos)*i,0);
		fread(&ex,sizeof(ST_Assuntos),1,arqLEI);
		
	}while(tecla!=27);
	
	fclose(arqLEI);
	
}

void Mod_Deleta_Logica(FILE*arq,char aux[],int param){
	
	Func_Muda_Tela();
	Gera_Tela_Cadastro(20,2,60,25,14,0);
	gotoxy(25,9);
	printf("__*Remover*__");
	
	switch(param){
	
		case 1:
			Func_Retira_usuario_Logica(arq,aux,param);
		break;
		
		case 2:
			Func_Retira_Palavra_Logica(arq,aux,param);
		break;
		
		case 3:
			if(Busca_Sentinela_Palavra_Assunto(Func_Tam(2),aux)==-1)
				Func_Retira_Assunto_Logica(arq,aux,param);
			else{
				
				gotoxy(26, 11);
	
				textcolor(12);
				printf("O Assunto Possui Dependencias");
				textcolor(14);
				
				_sleep(800);
		
			}
		break;
		
	}
	
}

void Func_Retira_Palavra_Logica(FILE*arqEXC,char chave[],int param){
	
	arqEXC = fopen( "Arquivo_Palavras.dat" , "rb+" );
	if(arqEXC!=NULL){
		
		int pos=Busca_Sentinela_Palavra(Func_Tam(param),chave);
		
		if(pos!=-1){
			ST_Palavras st;
			fseek(arqEXC,sizeof(ST_Palavras)*pos,0);
			fread(&st,sizeof(ST_Palavras),1,arqEXC);
			
			st.status=0;
			
			fseek(arqEXC,sizeof(ST_Palavras)*pos,0);
			fwrite(&st,sizeof(ST_Palavras),1,arqEXC);
		}else{
			gotoxy(26, 11);
		
			textcolor(12);
			printf("Inexistente");
			textcolor(14);
			
			_sleep(500);
		}
		
		fclose(arqEXC);
		
	}else{
		gotoxy(26, 11);
		
		textcolor(12);
		printf("Arquivo Inexistente");
		textcolor(14);
		
		_sleep(500);
	}
	
}


void Func_Retira_usuario_Logica(FILE*arqEXC,char chave[],int param){
	
	arqEXC = fopen( "Arquivo_usuarios.dat" , "rb+" );
	if(arqEXC!=NULL){
		
		int pos=Busca_Binario_usuario(Func_Tam(param),chave);
		
		if(pos!=-1){
			ST_usuarios st;
			fseek(arqEXC,sizeof(ST_usuarios)*pos,0);
			fread(&st,sizeof(ST_usuarios),1,arqEXC);
			
			st.status=0;
			
			fseek(arqEXC,sizeof(ST_usuarios)*pos,0);
			fwrite(&st,sizeof(ST_usuarios),1,arqEXC);
		}else{
			gotoxy(26, 11);
		
			textcolor(12);
			printf("Inexistente");
			textcolor(14);
			
			_sleep(500);
		}
		
		fclose(arqEXC);
		
	}else{
		gotoxy(26, 11);
		
		textcolor(12);
		printf("Arquivo Inexistente");
		textcolor(14);
		
		_sleep(500);
	}
	
}


void Func_Retira_Assunto_Logica(FILE*arqEXC,char chave[],int param){
	
	arqEXC = fopen( "Arquivo_Assuntos.dat" , "rb+" );
	if(arqEXC!=NULL){
		
		int pos=Busca_Exaustiva_Assunto(Func_Tam(param),chave);
		
		if(pos!=-1){
			ST_Assuntos st;
			fseek(arqEXC,sizeof(ST_Assuntos)*pos,0);
			fread(&st,sizeof(ST_Assuntos),1,arqEXC);
			
			st.status=0;
			
			fseek(arqEXC,sizeof(ST_Assuntos)*pos,0);
			fwrite(&st,sizeof(ST_Assuntos),1,arqEXC);
		}else{
			gotoxy(26, 11);
		
			textcolor(12); 
			printf("Inexistente");
			textcolor(14);
			
			_sleep(500);
		}
		
		fclose(arqEXC);
		
	}else{
		gotoxy(26, 11);
		
		textcolor(12);
		printf("Arquivo Inexistente");
		textcolor(14);
		
		_sleep(500);
	}
	
}



void Mod_Deleta_Fisica_Geral(FILE*arq,int param){
	
	switch(param){
	
		case 6:
			Func_Retira_usuario_Fisica_Geral(arq);
		break; 
		
		case 7:
			Func_Retira_Palavra_Fisica_Geral(arq);
		break;
		
		case 8:
			Func_Retira_Assunto_Fisica_Geral(arq);
		break;
		
	}
	
}

void Func_Retira_Palavra_Fisica_Geral(FILE*arqEXC){///ok
	
	arqEXC = fopen( "Arquivo_Palavras.dat" , "rb" );
	if(arqEXC!=NULL){
		FILE*arqTemp = fopen( "Temp_Palavra.dat" , "wb+" );
		
		ST_Palavras st;
	
		fread(&st,sizeof(ST_Palavras),1,arqEXC);
	
		while(!feof(arqEXC)){
			
			if(st.status!=0){
				fwrite(&st,sizeof(ST_Palavras),1,arqTemp);
			}
			fread(&st,sizeof(ST_Palavras),1,arqEXC);
			
		}
	
		fclose(arqTemp);
		fclose(arqEXC);
		
		remove("Arquivo_Palavras.dat");
		rename("Temp_Palavra.dat","Arquivo_Palavras.dat");
	}else{
		gotoxy(26, 11);
		
		textcolor(12);
		printf("Arquivo Inexistente");
		textcolor(14);
		
		_sleep(500);
	}
	
}

void Func_Retira_usuario_Fisica_Geral(FILE*arqEXC){///ok
	
	arqEXC = fopen( "Arquivo_usuarios.dat" , "rb" );
	if(arqEXC!=NULL){
		FILE*arqTemp = fopen( "Temp_usuarios.dat" , "wb+" );
		ST_usuarios st;
	
		fread(&st,sizeof(ST_usuarios),1,arqEXC);
	
		while(!feof(arqEXC)){
			
			if(st.status!=0){
				fwrite(&st,sizeof(ST_usuarios),1,arqTemp);
			}
			fread(&st,sizeof(ST_usuarios),1,arqEXC);
			
		}
	
		fclose(arqTemp);
		fclose(arqEXC);
		
		remove("Arquivo_usuarios.dat");
		rename("Temp_usuario.dat","Arquivo_usuarios.dat");
	}else{
		gotoxy(26, 11);
		
		textcolor(12);
		printf("Arquivo Inexistente");
		textcolor(14);
		
		_sleep(500);
	}
	
}


void Func_Retira_Assunto_Fisica_Geral(FILE*arqEXC){///ok
	
	arqEXC = fopen( "Arquivo_Assuntos.dat" , "rb" );
	if(arqEXC!=NULL){
		FILE*arqTemp = fopen( "Temp_Assuntos.dat" , "wb+" );
		ST_Assuntos st;
	
		fread(&st,sizeof(ST_Assuntos),1,arqEXC);
	
		while(!feof(arqEXC)){
			
			if(st.status!=0){
				fwrite(&st,sizeof(ST_Assuntos),1,arqTemp);
			}
			fread(&st,sizeof(ST_Assuntos),1,arqEXC);
			
		}
	
		fclose(arqTemp);
		fclose(arqEXC);
		
		remove("Arquivo_Assuntos.dat");
		rename("Temp_Assuntos.dat","Arquivo_Assuntos.dat");
	}else{
		gotoxy(26, 11);
		
		textcolor(12);
		printf("Arquivo Inexistente");
		textcolor(14);
		
		_sleep(500);
	}
	
}



void Gera_Tela_Intermediaria_Jogo(FILE*arq,ST_usuarios &ex,int pos){
	
	char tecla,i=4;
	
	Mod_Ranking(1,ex,arq);
	
	arq=fopen("Arquivo_usuarios.dat","rb");
	fseek(arq,sizeof(ST_usuarios)*pos,0);
	fread(&ex,sizeof(ST_usuarios),1,arq);
	fclose(arq);
	
	Func_Muda_Tela();
	Gera_Tela_Jogo(1,1,79,24,14,0,ex.level,ex.ponttotal,"Waiting...");
	Gera_Tela_Menu_Intermediario(12,7,55,15,14,0);
	Mod_Ranking(3,ex,arq);
	
	textcolor(15);
	
	Func_Controla_Menu(i,12,7);
	
	do{
	
		while(!kbhit()){
			Gera_Tela_Piscante(12,7,55,18,14,0);
		}
		tecla=getch();
		if(tecla==-32||tecla==0){
			tecla=getch();
		}
		
		if(tecla==13){
			if(i==4){
				Game_Engine(ex,pos,arq);
				clrscr();
			}else
			if(i==5){
				Func_Salvar(arq,ex,pos);
			}else{
				tecla=27;
			}	
		}else		
		if(tecla==80){
			i++;
			if(i>17){
				i=4;
			}else
			if(i>5){
				i=17;
			}
		}else
		if(tecla==72){
			i--;
			
			if(i<4){
				i=17;
			}else
			if(i>4){
				i=5;
			}
		}
		
		Func_Muda_Tela();
		Gera_Tela_Jogo(1,1,79,24,14,0,ex.level,ex.ponttotal,"Waiting...");
		Gera_Tela_Menu_Intermediario(12,7,55,15,14,0);
		Mod_Ranking(1,ex,arq);
		Mod_Ranking(3,ex,arq);
		
		
		textcolor(15);
		Func_Controla_Menu(i,12,7);
	
	}while(tecla!=27);
	
}


void Gera_Tela_Menu_Intermediario(unsigned char Quadroci,unsigned char Quadroli,unsigned char Quadrocf,unsigned char Quadrolf,unsigned char cort,unsigned char corf){

	Gera_Tela_Quadro(Quadroci,Quadroli,Quadrocf,Quadrolf+3,cort,corf);///QuadroMaior
	
	////////////////////////////////////////////////////////////////////////////////////
	Gera_Tela_Quadro(Quadroci+2,Quadroli+1,Quadrocf-2,Quadrolf-5,cort,corf);///QuadrosMenores
	gotoxy(Quadroci+3,Quadroli+2);printf("              Iniciar Jogo            ");
	
	Gera_Tela_Quadro(Quadroci+2,Quadroli+4,Quadrocf-2,Quadrolf-2,cort,corf);///QuadrosMenores
	gotoxy(Quadroci+3,Quadroli+5);printf("                 Salvar               ");
	
	Gera_Tela_Quadro(Quadroci+2,Quadroli+7,Quadrocf-2,Quadrolf+1,cort,corf);///QuadrosMenores
	gotoxy(Quadroci+3,Quadroli+8);printf("                  Sair                ");
	
	gotoxy(Quadroci+6,Quadroli+18);
	/////////////////////////////////////////////////////////////////////////////////////
	
}

void Func_Salvar(FILE*arq,ST_usuarios ex,int pos){
	
	arq=fopen("Arquivo_usuarios.dat","rb+");
	fseek(arq,sizeof(ST_usuarios)*pos,0);
	fwrite(&ex,sizeof(ST_usuarios),1,arq);
	fclose(arq);
	
};

void Game_Engine(ST_usuarios &ex,int pos,FILE*arq){
	
	char o,palavra[30];
	int i,posp;
	
	Func_Muda_Tela();
	
	do{
		
		posp=rand()%(Func_Tam(2));///verifica��o
		Func_Conta_Letras(palavra,posp,i,arq);
		
		clrscr();
		Gera_Tela_Jogo(1,1,79,24,14,0,ex.level,ex.ponttotal,palavra);
		
		Main_Controle_Jogo(ex,pos,posp,i,palavra,o,arq);

		
	}while(o!=27);	
	
}

void Func_Conta_Letras(char palavra[],int posp,int &i,FILE*arq){
	
	ST_Palavras st;
	
	arq=fopen("Arquivo_Palavras.dat","rb");
	fseek(arq,sizeof(ST_Palavras)*(posp),0);
	fread(&st,sizeof(ST_Palavras),1,arq);
	fclose(arq);
	
	strcpy(palavra,st.palavra);
	for(i=0;palavra[i]!='\0'&&i<30;i++);
	
}


void Main_Controle_Jogo(ST_usuarios &ex,int pos,int posp,int i,char palavra[],char &o,FILE*arq){
	
	int posl;
	char y;
	do{
		
		textcolor(rand()%16);
		posl=rand()%(i);
		
		Gera_Tela_Level(1,1,ex.level);
		Gera_Tela_Pontuacao(1,1,ex.ponttotal);
		Mod_Ranking(3,ex,arq);
		
		
		y=((rand()%13)+5);
		
		Alt_Controle_Jogo(ex,((rand()%52)+7),y,palavra[posl],pos,o,arq);
		Controle_Pontuacao(ex,pos,i,posl,posp,y,palavra,arq);

	}while(o!=27&&i>0);

}


void Gera_Tela_Level(unsigned char Quadroci,unsigned char Quadroli,unsigned char level){
	
	textcolor(15);
	gotoxy(Quadroci+61,Quadroli+8);printf("*%3d*",level);
	textcolor(14);
	
}


void Gera_Tela_Pontuacao(unsigned char Quadroci,unsigned char Quadroli,int pontuacao){
	
	textcolor(15);
	gotoxy(Quadroci+61,Quadroli+5);printf("*%10d*",pontuacao);
	textcolor(14);
	
}

void Controle_Pontuacao(ST_usuarios &ex,int pos,int &i,int posl,int posp,char controle,char palavra[],FILE*arq){
	
	
	ST_Palavras st;
	arq=fopen("Arquivo_Palavras.dat","rb+");
	fseek(arq,sizeof(ST_Palavras)*posp,0);
	fread(&st,sizeof(ST_Palavras),1,arq);
	
	
	if(controle<18){	
			ex.ponttotal+=200000;
			int aux=Busca_Letra(st.palavra,palavra[posl],st.letrasativas);
			st.letrasativas[aux]=80;///vai dar merda
			///fseek(arq,sizeof(ST_Palavras)*posp,0);
			fwrite(&st,sizeof(ST_Palavras),1,arq);
			fclose(arq);
			
			Mod_Ranking(2,ex,arq);//(ex,pos,arq);
			Gera_Tela_Palavra(1,1,st.palavra,aux);
			Func_Retira_Letra(posl,i,palavra);
			
		
	}else{
			ex.ponttotal-=1000;
			fclose(arq);
	}
	
	_sleep(150);
	
}

int Busca_Letra(char nome[],char letra,int letrasativas[]){
	
	int i=0;
		
	for(;i<TAM_PAL&&letrasativas[i]!=80&&nome[i]!=letra;){
		i++;
	}
	
	if(i<TAM_PAL&&letrasativas[i]!=80){
		return i;
	}
		
	return -1;
}


void Func_Retira_Letra(int posl,int &i,char palavra[]){
	
	for(unsigned char j=posl;j<i;j++){
		palavra[j]=palavra[j+1];	
	}
	
	i--;
	
}


void Controle_Maior_Rank(ST_usuarios &ex,int pos,FILE*arq){///Nulo
	
	ST_Ranking st;
	arq=fopen("Arquivo_Ranking.dat","rb");
	fread(&st,sizeof(ST_Ranking),1,arq);
	fclose(arq);
	
	if(ex.ponttotal>st.ponttotal){
		
		st.ponttotal=ex.ponttotal;
		strcpy(st.login,ex.login);
		
	}
					
}




void Alt_Controle_Jogo(ST_usuarios &ex,char x,char &y,char Letra,int pos,char &o,FILE*arq){

	int z;
	
	do{
		///textcolor(z);
		z=(rand()%10)+6;
		textcolor(z);
		gotoxy(x,y);
		printf("%c",Letra);
		
		while(!kbhit()&&y<18){
			
			Func_Limpa_Rastro_Letra(x,y,z);
			
			y++;
			gotoxy(x,y);
			printf("%c",Letra);
			ex.level=Func_Level(Tabela_Pontuacao(ex.ponttotal));
		}
		
		Func_Limpa_Rastro_Letra(x,y,z);
		
		if(y<18){
			o=getch();
		}else{
			o=18;
		}
		
	
	}while(o!=Letra&&o!=27&&y<18);
	
}


unsigned char Tabela_Pontuacao(int pontuacao){
	
	unsigned char lev=1;

	if(pontuacao>10000){
		lev=2;
		
		if(pontuacao>100000){
			lev=3;
			
			if(pontuacao>1000000){
				lev=4;
				
				if(pontuacao>10000000){
					lev=5;
					
				}
				
			}
			
		}
		
	}
	
	return lev;
	
}



void Func_Limpa_Rastro_Letra(char x,char y,int z){
	
	gotoxy(x,y);
	printf(" ");
	textcolor(z);

}


unsigned char Func_Level(unsigned char lev){
	
	int aux=(lev+800-((lev+120)*lev));
	
	if(aux<=0){
		aux=1;
	}
	
	_sleep(aux);
	return lev;
	
}




void Gera_Tela_Jogo(unsigned char Quadroci,unsigned char Quadroli,unsigned char Quadrocf,unsigned char Quadrolf,unsigned char cort,unsigned char corf,unsigned char level,int pontuacao,char nome[]){
	
	
						///Quadro Maior;
	Gera_Tela_Quadro(Quadroci,Quadroli,Quadrocf,Quadrolf,cort,corf);
	Gera_Tela_Quadro(Quadroci+5,Quadroli+3,Quadrocf-20,Quadrolf-5,cort,corf);
	
	textcolor(10);
	gotoxy(Quadroci+31,Quadroli+1);printf("*__Game__*");
	Gera_Tela_Palavra(Quadroci,Quadroli,nome,-1);
	
	
						///Quadro Op��es;
	Gera_Tela_Quadro(Quadroci+5,Quadroli+20,Quadrocf-20,Quadrolf-1,cort,corf);
	
	textcolor(12);
	gotoxy(Quadroci+8,Quadroli+21);printf("(ESC)-Para Sair");
	
						///Quadro Op��es 2;
	Gera_Tela_Quadro(Quadroci+60,Quadroli+3,Quadrocf-5,Quadrolf-1,cort,corf);
	
	textcolor(11);
	gotoxy(Quadroci+61,Quadroli+1);printf(" *Opcoes*");
	
	textcolor(13);
	gotoxy(Quadroci+61,Quadroli+4);printf("*Pontuacao:*");
	Gera_Tela_Pontuacao(Quadroci,Quadroli,pontuacao);
	
	textcolor(13);
	gotoxy(Quadroci+61,Quadroli+7);printf("  *Level:*");
	Gera_Tela_Level(Quadroci,Quadroli,level);
	
	textcolor(13);
	gotoxy(Quadroci+61,Quadroli+10);printf("*MaiorRank*");
	gotoxy(Quadroci+61,Quadroli+13);printf("*Pontos*");
	//Gera_Tela_Rank(Quadroci,Quadroli,ex);
	
	textcolor(14);
	
	
}



void Gera_Tela_Palavra(unsigned char Quadroci,unsigned char Quadroli,char nome[],int pos){
	
	if(pos>-1){
		
		textcolor(11);
		gotoxy(Quadroci+11+pos,Quadroli+2);printf("%c",nome[pos]);
		
	}else{
		
		textcolor(10);
		gotoxy(Quadroci+11,Quadroli+2);printf("%s",nome);	
		
	}
	
	textcolor(14);
	
}




void Ordenacao_Direta_usuario(int Tam){//ok
	ST_usuarios st[Tam],aux;
	int P;P=Tam-1;
	
	FILE*arqORDIR=fopen("Arquivo_usuarios.dat","rb+");
	fread(&st,sizeof(ST_usuarios),Tam,arqORDIR);
	
	while (P>0 && stricmp(st[P].login,st[P-1].login)<0){
		aux = st[P];
		st[P] = st[P-1];
		st[P-1] = aux;
		P--;
		/*
		
		fseek(arq,sizeof(ST_usuarios)*p,0);
		fread(&aux,sizeof(ST_usuarios),1,arq);
		fseek(arq,sizeof(ST_usuarios)*p-1,0);
		fread(&st,sizeof(ST_usuarios),1,arq);
		fwrite(&aux,sizeof(ST_usuarios),1,arq);
		fwrite(&st,sizeof(ST_usuarios),1,arq);
		
		*/
	}
	
	fseek(arqORDIR,0,0);
	fwrite(&st,sizeof(ST_usuarios),Tam,arqORDIR);
	fclose(arqORDIR);
}

void OrdenaBolha_Assunto(int Tam){///ok
	ST_Assuntos st[Tam],aux;
	int b,qtde=Tam;
	FILE*arqBOL=fopen("Arquivo_Assuntos.dat","rb+");
	fread(&st,sizeof(ST_Assuntos),Tam,arqBOL);
	
 	
	while(qtde>0){
	  for(b=0;b<qtde-1;b++)
	   if(stricmp(st[b].codassunto,st[b+1].codassunto)>0){ 
	     	aux = st[b];
			st[b] = st[b+1];
			st[b+1] = aux; 
	   } 
	   qtde--; 
	} 
	
	fseek(arqBOL,0,0);
	fwrite(&st,sizeof(ST_Assuntos),Tam,arqBOL);
	fclose(arqBOL);
}

void Ordenacao_Direta_Ranking(int Tam){//ok
	ST_Ranking st[Tam],aux;
	int P;P=Tam-1;
	
	FILE*arqORDIR=fopen("Arquivo_Ranking.dat","rb+");
	fread(&st,sizeof(ST_Ranking),Tam,arqORDIR);
	
	while (P>0 && st[P].ponttotal>st[P-1].ponttotal){
		aux = st[P];
		st[P] = st[P-1];
		st[P-1] = aux;
		P--;
	}
	
	fseek(arqORDIR,0,0);
	fwrite(&st,sizeof(ST_Ranking),Tam,arqORDIR);
	fclose(arqORDIR);
}

void OrdenaBolha_Palavra(int Tam){///ok
	ST_Palavras st[Tam],aux;
	int b,qtde=Tam;
	FILE*arqBOL=fopen("Arquivo_Palavras.dat","rb+");
	fread(&st,sizeof(ST_Palavras),Tam,arqBOL);
	
 	
	while(qtde>0){
	  for(b=0;b<qtde-1;b++)
	   if(stricmp(st[b].codpalavra,st[b+1].codpalavra)>0){ 
	     	aux = st[b];
			st[b] = st[b+1];
			st[b+1] = aux; 
	   } 
	   qtde--; 
	} 
	
	fseek(arqBOL,0,0);
	fwrite(&st,sizeof(ST_Palavras),Tam,arqBOL);
	fclose(arqBOL);
}

char Busca_Binario_usuario(int Tam,char chave[]){
	ST_usuarios st[Tam];
	int inicio=0,fim=Tam-1,meio; meio=fim/2;
	
	FILE*arqBIN=fopen("Arquivo_usuarios.dat","rb");
	fread(&st,sizeof(ST_usuarios),Tam,arqBIN);
	fclose(arqBIN);
		
	while (inicio<fim && stricmp(chave,st[meio].login)!=0){
		if (stricmp(st[meio].login,chave)<0){
			inicio = meio + 1;
		}		
		else fim = meio;
		meio = (inicio+fim)/2;	
	}
	if (stricmp(chave,st[meio].login)==0){
		return meio;
	}	
	return -1;
	
	
}

char Busca_Sequencial_Indexada_Ranking(int Tam,char chave[]){
	ST_Ranking st[Tam];
	char i=0;
	
	FILE*arqSEQIN=fopen("Arquivo_Ranking.dat","rb");
	fread(&st,sizeof(ST_Ranking),Tam,arqSEQIN);
	fclose(arqSEQIN);

	while(i<Tam&&stricmp(chave,st[i].login)>0)
		i++;
	if(i<Tam&&stricmp(chave,st[i].login)==0)
		return i;
	return -1;
}

char Busca_Exaustiva_Assunto(int Tam,char chave[]){
	ST_Assuntos st[Tam];
	char i=0;
	
	FILE*arqBEX=fopen("Arquivo_Assuntos.dat","rb");
	fread(&st,sizeof(ST_Assuntos),Tam,arqBEX);
	fclose(arqBEX);

	while(i<Tam&&stricmp(chave,st[i].codassunto)!=0)
		i++;
	if(i<Tam)
		return i;
	return -1;
}

char Busca_Sentinela_Palavra(int Tam,char chave[]){
	ST_Palavras st[Tam+1];
	char i=0;
	
	FILE*arqBS=fopen("Arquivo_Palavras.dat","rb");
	fread(&st,sizeof(ST_Palavras),Tam,arqBS);
	fclose(arqBS);

	strcpy(st[Tam].codpalavra,chave) ;
	while(stricmp(chave,st[i].codpalavra)!=0)
		i++;
	if(i<Tam)
		return i;
	return -1;

}

char Busca_Sentinela_Palavra_Assunto(int Tam,char chave[]){
	ST_Palavras st[Tam+1];
	char i=0;
	
	FILE*arqBS=fopen("Arquivo_Palavras.dat","rb");
	fread(&st,sizeof(ST_Palavras),Tam,arqBS);
	fclose(arqBS);

	strcpy(st[Tam].codassunto,chave) ;
	while(stricmp(chave,st[i].codassunto)!=0)
		i++;
	if(i<Tam)
		return i;
	else
		return -1;

}

int main(void){
	srand((unsigned)(time(NULL)));
	FILE*arq=NULL;
	//Mod_Inserir(arq,1);
	///Mod_Altera(arq,2);
	///Mod_Deleta_Fisica(arq,2);
	//Mod_Deleta_Logica(arq,2);
	//Mod_Exibe(arq,2);
	//Mod_Exibe(arq,1);
	//Mod_Exibe(arq,3);
	
	///Gera_Tela_Intermediaria_Menu_Alt(arq,1);
	Main_Menu();
	/*
	ST_usuarios ex;
	
	int pos=2;
	arq=fopen("Arquivo_usuarios.dat","rb");
	fseek(arq,sizeof(ST_usuarios)*pos,0);
	fread(&ex,sizeof(ST_usuarios),1,arq);
	fclose(arq);
	
	
	Gera_Tela_Intermediaria_Jogo(arq,ex,pos);
	*/
	//Menu_Adm(arq);
	//Mod_Ranking(1,NULL);
	//Mod_Ranking(2,"1");
	
	///Mod_Exibe(arq,4);
	
	return 0;
}


void Mod_Ranking(char opc,ST_usuarios ex,FILE*arq){
	FILE*arqRANK;
	
	ST_Ranking st;
	
	switch(opc){
		case 1:
			arq=fopen("Arquivo_usuarios.dat","rb");
			Mod_Ranking_Criacao(arqRANK,arq,st,ex);
		break;
		
		case 2:
			Mod_Ranking_Alteracao(arqRANK,st,ex.login,ex);
		break;
		
		case 3:
			Exibe_Top_Ranking(arqRANK,st);
		break;
		
		
	}
	
}

void Mod_Ranking_Criacao(FILE*arqRANK,FILE*arq,ST_Ranking st,ST_usuarios ex){
	
	arqRANK=fopen("Arquivo_Ranking.dat","wb+");
	if(arq!=NULL){
		while(!feof(arq)){
			
			fread(&ex,sizeof(ST_usuarios),1,arq);
	
			strcpy(st.login,ex.login);
			st.status=ex.status;
			st.ponttotal=ex.ponttotal;
			
			fwrite(&st,sizeof(ST_Ranking),1,arqRANK);
			
			fclose(arqRANK);
			Ordenacao_Direta_Ranking(Func_Tam(4));
			
			arqRANK=fopen("Arquivo_Ranking.dat","ab+");
			fseek(arqRANK,0,2);
			
		}
	}
	fclose(arq);		
	
}

void Exibe_Top_Ranking(FILE*arqRANK,ST_Ranking st){
	
	arqRANK=fopen("Arquivo_Ranking.dat","rb");
	
	char flag=0;
	while(!feof(arqRANK)&&flag!=1){
		fread(&st,sizeof(ST_Ranking),1,arqRANK);
		if(st.status==1){
			flag=1;
		}
	}	
		
	Gera_Tela_Rank(1,1,st);
	fclose(arqRANK);
	
}


void Mod_Ranking_Alteracao(FILE*arqRANK,ST_Ranking st,char login[],ST_usuarios ex){
		
		arqRANK=fopen("Arquivo_Ranking.dat","rb+");
		FILE*arqTEMP=fopen("TEMP_RANKING.dat","wb+");

		
		fread(&st,sizeof(ST_Ranking),1,arqRANK);
		while(!feof(arqRANK)){
			if(stricmp(st.login,login)!=0){
				fwrite(&st,sizeof(ST_Ranking),1,arqTEMP);
			}
			fread(&st,sizeof(ST_Ranking),1,arqRANK);
		}
		
		strcpy(st.login,ex.login);
		st.ponttotal=ex.ponttotal;
		st.status=ex.status;
		fwrite(&st,sizeof(ST_Ranking),1,arqTEMP);
		
		fclose(arqRANK);
		fclose(arqTEMP);
		remove("Arquivo_Ranking.dat");
		rename("TEMP_RANKING.dat","Arquivo_Ranking.dat");
		
		Ordenacao_Direta_Ranking(Func_Tam(4));
		
}

void Alt_Exibe_Ranking(FILE*arqLEI,int tl){
	
	arqLEI=fopen("Arquivo_Ranking.dat","rb");

	ST_Ranking ex;	
	char tecla;int i=0,comeco;
	
	fread(&ex,sizeof(ST_Ranking),1,arqLEI);
	while(ex.status==0){
		fread(&ex,sizeof(ST_Ranking),1,arqLEI);
		i++;
	}
	comeco=i;
	do{
		
		
		if(ex.status!=0){
			
			Func_Muda_Tela();
			Gera_Tela_Leitura_Ranking(20,2,60,25,14,0,ex,i);
			while(!kbhit()){
				Gera_Tela_Piscante(20,2,60,25-21,14,0);
			}
			tecla=getch();
			if(tecla==-32||tecla==0){
				tecla=getch();
			}
		}else
		if(i==tl-1||i<=comeco){
			if(i<=comeco){
				i++;
			}
			tecla=getch();
			if(tecla==-32||tecla==0){
				tecla=getch();
			}
		}
		
		if(i>0&&tecla==75){
			i--;	
		}else
		if(i<tl-1&&tecla==77){
			i++;
		}
		
		fseek(arqLEI,sizeof(ST_Ranking)*i,0);
		fread(&ex,sizeof(ST_Ranking),1,arqLEI);
		
	}while(tecla!=27);
	
	fclose(arqLEI);
	
}

void Gera_Tela_Rank(unsigned char Quadroci,unsigned char Quadroli,ST_Ranking ex){
	
	
	textcolor(0);
	gotoxy(Quadroci+61,Quadroli+11);printf("            ");
	textcolor(15);
	gotoxy(Quadroci+61,Quadroli+11);printf("*%s*",ex.login);
	textcolor(0);
	gotoxy(Quadroci+61,Quadroli+14);printf("            ");
	textcolor(15);
	gotoxy(Quadroci+61,Quadroli+14);printf("*%10d*",ex.ponttotal);
	textcolor(14);
	
	
}


